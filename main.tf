terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  profile = "default"
  region  = var.region
}

# Create a VPC
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "Example-Project-VPC"
  }

}

# subnet
resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Example-Project-Subnet"
  }
}

# IGW
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main-IGW"
  }
}


# Instance
resource "aws_instance" "web" {
  ami           = "ami-03e88be9ecff64781"
  instance_type = "t3.micro"
  subnet_id     = aws_subnet.main.id
  private_ip    = "10.0.1.6"

  tags = {
    Name = "example-project-instance"
  }
}


resource "aws_eip" "main" {
  vpc = true

  instance                  = aws_instance.web.id
  associate_with_private_ip = "10.0.1.6"
  depends_on                = [aws_internet_gateway.gw]
}


resource "aws_route_table" "example" {
  vpc_id = aws_vpc.main.id


  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "example-route-table"
  }
}


resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.main.id
  route_table_id = aws_route_table.example.id
}